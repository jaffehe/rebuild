/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.support.setup;

import com.rebuild.core.Application;
import com.rebuild.core.support.RebuildConfiguration;

import java.io.File;


public interface InstallState {

    
    String INSTALL_FILE = ".rebuild";

    
    default boolean checkInstalled() {
        return Application.devMode() || getInstallFile().exists();
    }

    
    default File getInstallFile() {
        return RebuildConfiguration.getFileOfData(INSTALL_FILE);
    }
}
