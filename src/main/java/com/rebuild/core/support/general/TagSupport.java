/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.support.general;

import cn.devezhao.persist4j.Field;
import cn.devezhao.persist4j.engine.ID;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.core.Application;
import com.rebuild.core.RebuildException;
import com.rebuild.core.metadata.easymeta.EasyField;
import com.rebuild.core.metadata.impl.EasyFieldConfigProps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ArrayUtils;

import java.util.HashMap;
import java.util.Map;


@Slf4j
public class TagSupport {

    
    public static String[] items(Field field, ID recordId) {
        if ((int) field.getOwnEntity().getEntityCode() != recordId.getEntityCode()) {
            throw new RebuildException("Bad id for found tag-value : " + field);
        }

        Object[][] array = Application.getPersistManagerFactory().createQuery(
                "select tagName from TagItem where belongField = ? and recordId = ? order by seq")
                .setParameter(1, field.getName())
                .setParameter(2, recordId)
                .array();
        if (array.length == 0) return ArrayUtils.EMPTY_STRING_ARRAY;

        String[] tagNames = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            tagNames[i] = (String) array[i][0];
        }
        return tagNames;
    }

    
    public static String[] items(String fieldPath, ID recordId) {
        Object[] last = N2NReferenceSupport.getLastObject(fieldPath, recordId);
        return items((Field) last[0], (ID) last[1]);
    }

    
    public static Map<String, String> getNamesColor(EasyField field) {
        Map<String, String> colors = new HashMap<>();

        JSONArray options = field.getExtraAttrs().getJSONArray(EasyFieldConfigProps.TAG_LIST);
        if (options != null) {
            for (Object o : options) {
                JSONObject item = (JSONObject) o;
                colors.put(item.getString("name"), item.getString("color"));
            }
        }
        return colors;
    }
}
