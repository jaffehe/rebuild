/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.trigger.aviator;

import com.googlecode.aviator.AviatorEvaluator;
import com.googlecode.aviator.AviatorEvaluatorInstance;
import com.googlecode.aviator.Options;
import com.googlecode.aviator.exception.ExpressionSyntaxErrorException;
import com.googlecode.aviator.lexer.token.OperatorType;
import com.googlecode.aviator.runtime.function.system.AssertFunction;
import com.googlecode.aviator.runtime.type.AviatorFunction;
import com.googlecode.aviator.runtime.type.Sequence;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;


@Slf4j
public class AviatorUtils {

    private static final AviatorEvaluatorInstance AVIATOR = AviatorEvaluator.newInstance();

    static {
        
        AVIATOR.setOption(Options.ALWAYS_PARSE_FLOATING_POINT_NUMBER_INTO_DECIMAL, Boolean.TRUE);
        AVIATOR.setOption(Options.ENABLE_PROPERTY_SYNTAX_SUGAR, Boolean.FALSE);
        AVIATOR.setOption(Options.ALLOWED_CLASS_SET, Collections.emptySet());
        AVIATOR.setOption(Options.TRACE_EVAL, Boolean.FALSE);

        try {
            
            AVIATOR.addStaticFunctions("StringUtils", StringUtils.class);
        } catch (Exception ignored) {
        }

        
        AVIATOR.addOpFunction(OperatorType.ADD, new OverDateOperator.DateAdd());
        AVIATOR.addOpFunction(OperatorType.SUB, new OverDateOperator.DateSub());
        AVIATOR.addOpFunction(OperatorType.LE, new OverDateOperator.DateCompareLE());
        AVIATOR.addOpFunction(OperatorType.LT, new OverDateOperator.DateCompareLT());
        AVIATOR.addOpFunction(OperatorType.GE, new OverDateOperator.DateCompareGE());
        AVIATOR.addOpFunction(OperatorType.GT, new OverDateOperator.DateCompareGT());
        AVIATOR.addOpFunction(OperatorType.EQ, new OverDateOperator.DateCompareEQ());
        AVIATOR.addOpFunction(OperatorType.NEQ, new OverDateOperator.DateCompareNEQ());

        
        addCustomFunction(new DateDiffFunction());
        addCustomFunction(new DateAddFunction());
        addCustomFunction(new DateSubFunction());
        addCustomFunction(new CurrentUserFunction());
        addCustomFunction(new CurrentBizunitFunction());
        addCustomFunction(new CurrentDateFunction());
        addCustomFunction(new ChineseYuanFunction());
        addCustomFunction(new TextFunction());
        addCustomFunction(new IsNullFunction());
    }

    
    public static Object eval(String expression) {
        return eval(expression, null, false);
    }

    
    public static Object eval(String expression, Map<String, Object> env) {
        return eval(expression, env, false);
    }

    
    public static Object eval(String expression, Map<String, Object> env, boolean quietly) {
        try {
            return AVIATOR.execute(expression, env == null ? Collections.emptyMap() : env);
        } catch (Exception ex) {
            if (ex instanceof AssertFunction.AssertFailed) {
                throw new AssertFailedException((AssertFunction.AssertFailed) ex);
            }

            log.error("Bad aviator expression : \n>> {}\n>> {}\n>> {}", expression, env, ex.getLocalizedMessage());
            if (!quietly) throw ex;
        }
        return null;
    }

    
    public static boolean validate(String expression) {
        try {
            getInstance().validate(expression);
            return true;
        } catch (ExpressionSyntaxErrorException ex) {
            log.warn("Bad aviator expression : `{}`", expression);
            return false;
        }
    }

    
    public static void addCustomFunction(final AviatorFunction function) {
        log.info("Add custom function : {}", function);
        AVIATOR.addFunction(function);
    }

    
    public static AviatorEvaluatorInstance getInstance() {
        return AVIATOR;
    }

    
    @SuppressWarnings("unchecked")
    public static Iterator<Object> toIterator(Object value) {
        if (value instanceof Collection) return ((Collection<Object>) value).iterator();
        if (value instanceof Sequence) return ((Sequence<Object>) value).iterator();

        throw new UnsupportedOperationException("Unsupport type : " + value);
    }
}
