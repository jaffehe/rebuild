/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.dashboard.charts.builtin;

import cn.devezhao.persist4j.engine.ID;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.utils.JSONUtils;


public interface BuiltinChart {

    
    default String getChartType() {
        return this.getClass().getSimpleName();
    }

    
    default JSONObject getChartConfig() {
        
        return JSONUtils.toJSONObject(new String[]{"entity", "type"}, new String[]{"User", getChartType()});
    }

    
    ID getChartId();

    
    String getChartTitle();
}
