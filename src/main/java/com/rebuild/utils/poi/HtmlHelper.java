

package com.rebuild.utils.poi;

import java.util.Formatter;

import org.apache.poi.ss.usermodel.CellStyle;


public interface HtmlHelper {
    
    void colorStyles(CellStyle style, Formatter out);
}
